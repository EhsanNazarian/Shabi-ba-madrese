var socket = io();

var data;

$(document).ready(function () {
    socket.on('data', function (_data) {
        console.log('on data');
        data = _data;

        updateProducer(data);
        updateConsumer(data);
        $('#current_month').text(data.currentMonth);
    });
});

function updateProducer(data) {
    var producers = data.producers;

    var producerTableData = [];

    for (var i in producers) {
        var producer = producers[i];
        var row = {
            name: producer.name,
            money: producer.money,
            a: producer.productCounts.a,
            b: producer.productCounts.b,
            c: producer.productCounts.c
        };
        producerTableData.push(row);
    }

    var dynatable = $('#producers').dynatable({
        dataset: {
            records: producerTableData
        },
        features: {
            paginate: false,
            recordCount: false,
            search: false
        }
    }).data('dynatable');

    dynatable.settings.dataset.originalRecords = producerTableData;
    dynatable.process();
}

function updateConsumer(data) {
    var prices = getPrices(data);

    var consumers = data.consumers;

    var consumerTableData = [];

    for (var i in consumers) {
        var consumer = consumers[i];
        var a = consumer.productCounts.a;
        var b = consumer.productCounts.b;
        var c = consumer.productCounts.c;
        var row = {
            name: consumer.name,
            money: consumer.money,
            a: a,
            b: b,
            c: c,
            score: a * prices.a + b * prices.b + c * prices.c
        };
        consumerTableData.push(row);
    }

    var dynatable = $('#consumers').dynatable({
        dataset: {
            records: consumerTableData
        },
        features: {
            paginate: false,
            recordCount: false,
            search: false
        }
    }).data('dynatable');

    dynatable.settings.dataset.originalRecords = consumerTableData;
    dynatable.process();
}


function getPrices(data) {
    var prices = {
        a: 0,
        b: 0,
        c: 0
    };

    var tmp = {
        a: {
            totalCount: 0,
            totalCost: 0
        },
        b: {
            totalCount: 0,
            totalCost: 0
        },
        c: {
            totalCount: 0,
            totalCost: 0
        }
    };

    for(var i = data.currentMonth; i >= 0 && i > data.currentMonth - 3; i--) {
        var overall = data.months[i].overall;
        tmp.a.totalCost += overall.a.totalCost;
        tmp.b.totalCost += overall.b.totalCost;
        tmp.c.totalCost += overall.c.totalCost;

        tmp.a.totalCount += overall.a.totalCount;
        tmp.b.totalCount += overall.b.totalCount;
        tmp.c.totalCount += overall.c.totalCount;
    }
    if(tmp.a.totalCount == 0) {
        prices.a = 0;
    }
    else {
        prices.a = tmp.a.totalCost / tmp.a.totalCount;
    }

    if(tmp.b.totalCount == 0) {
        prices.b = 0;
    }
    else {
        prices.b = tmp.b.totalCost / tmp.b.totalCount;
    }

    if(tmp.c.totalCount == 0) {
        prices.c = 0;
    }
    else {
        prices.c = tmp.c.totalCost / tmp.c.totalCount;
    }


    return prices;
}
