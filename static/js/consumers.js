var socket = io();

var data;

$(document).ready(function () {
    socket.on('data', function (_data) {
        data = _data;
        var producers = data.producers;
        var consumers = data.consumers;
        var i;
        var $producers = $('#producers');
        for (i in producers) {
            var producer = producers[i];
            var $option = $('<option/>').val(producer.id).text(producer.name);
            $producers.append($option);
        }

        var $consumers = $('#consumers');
        for (i in consumers) {
            var consumer = consumers[i];
            var $option = $('<option/>').val(consumer.id).text(consumer.name);
            $consumers.append($option);
        }
    });

    $('#submit').click(function () {
        submit();
    });
});


function setC() {
    var producers = data.producers;
    var producer;
    var id = $('#select').val();
    var find = false;

    for (var i in producers) {
        producer = producers[i];
        if (producer.id === id) {
            find = true;
            break;
        }
    }

    if (!find) {
        return;
    }

    var remainResource = data.resourcePerMonth;
    remainResource -= producer.productsRequiredResource.a * $('#a').val();
    remainResource -= producer.productsRequiredResource.b * $('#b').val();
    $('#c').val(Math.floor(remainResource / producer.productsRequiredResource.c));
}

function submit() {
    var producers = data.producers;
    var producer;
    var producerId = $('#producers').val();
    if(!producerId) {
        alert('تیم تولید‌کننده را انتخاب کنید!');
        return;
    }
    var consumerId = $('#consumers').val();
    if(!consumerId) {
        alert('تیم خریدار را انتخاب کنید!');
        return;
    }

    var find = false;
    var i;

    for (i in producers) {
        producer = producers[i];
        if (producer.id === producerId) {
            find = true;
            break;
        }
    }

    if (!find) {
        return;
    }


    var consumers = data.consumers;
    var consumer;
    find = false;

    for (i in consumers) {
        consumer = consumers[i];
        if (consumer.id === consumerId) {
            find = true;
            break;
        }
    }

    if (!find) {
        return;
    }

    var productCounts = producer.productCounts;


    var a_cost = $('#a_cost').val(), b_cost = $('#b_cost').val(), c_cost = $('#c_cost').val();
    var a_count = $('#a_count').val(), b_count = $('#b_count').val(), c_count = $('#c_count').val();

    if (productCounts.a < a_count || productCounts.b < b_count || productCounts.c < c_count) {
        alert('کالای کافی برای فروش وجود ندارد!');
        return;
    }

    if (a_cost * a_count + b_cost * b_count + c_cost * c_count > consumer.money) {
        alert('پول کافی برای خرید کالا وجود ندارد!');
        return;
    }


    var transaction = {producerId: producerId, a_cost: a_cost, b_cost: b_cost, c_cost: c_cost,
        consumerId: consumerId, a_count: a_count, b_count: b_count, c_count: c_count
    };
    socket.emit('transaction', transaction);
}
