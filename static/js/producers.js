var socket = io();

var data;

$(document).ready(function () {
    socket.on('data', function (_data) {
        data = _data;
        var producers = data.producers;
        var i;
        var $select = $('#select');
        for (i in producers) {
            var producer = producers[i];
            var $option = $('<option/>').val(producer.id).text(producer.name);
            $select.append($option);
        }
    });

    $('input').change(function () {
        setC();
    });

    $('#select').change(function () {
        setC();
    });

    $('#submit').click(function () {
        submit();
    });
});


function setC() {
    var producers = data.producers;
    var producer;
    var id = $('#select').val();
    var find = false;

    for (var i in producers) {
        producer = producers[i];
        if (producer.id === id) {
            find = true;
            break;
        }
    }

    if (!find) {
        return;
    }

    var remainResource = data.resourcePerMonth;
    remainResource -= producer.productsRequiredResource.a * $('#a').val();
    remainResource -= producer.productsRequiredResource.b * $('#b').val();
    $('#c').val(Math.floor(remainResource / producer.productsRequiredResource.c));
}

function submit() {
    var a = $('#a').val(), b = $('#b').val(), c = $('#c').val();

    if (a < 0 || b < 0 || c < 0) {
        alert('اعداد وارد شده باید مثبت باشند!');
        return;
    }
    var id = $('#select').val();

    if(!id) {
        alert('یک تیم را انتخاب کنید!');
        return;
    }

    var plan = {id: id, a: a, b: b, c: c};
    socket.emit('plan', plan);
}
