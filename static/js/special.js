var socket = io();

$(document).ready(function () {

    $.ajax({
        type: 'GET',
        url: '/static/template/template.html',
        success: function (file_html) {
            // success
            var template = swig.compile(file_html);

            socket.on('data', function (data) {
                var output = template({
                    pagename: 'awesome people',
                    producers: data.producers
                });
                $('#test').append(output);
                console.log(data);
            });
        }
    });
});
