var express = require('express');
var app = express();
var httpServer = require('http').Server(app);
var io = require('socket.io')(httpServer);
var storage = require('node-persist');
const DATA = 'data.json';

//------------------------------------------------------------- data

//you must first call storage.init or storage.initSync
storage.initSync({
    dir: __dirname + '/data',
    stringify: JSON.stringify,
    parse: JSON.parse,
    encoding: 'utf8',
    logging: false,  // can also be custom logging function
    continuous: true,
    interval: false,
    ttl: false, // ttl* [NEW], can be true for 24h default or a number in MILLISECONDS
});

var data = storage.getItem(DATA);

if (!data) {
    data = {
        producers: [
            {
                "id": "p1",
                "name": "ali",
                "producePlan": {
                    "a": 3,
                    "b": 1,
                    "c": 0
                },
                "productsRequiredResource": {
                    "a": 100,
                    "b": 200,
                    "c": 300
                },
                "productCounts": {
                    "a": 19,
                    "b": 25,
                    "c": 30
                },
                "money": 2980
            }
        ],
        consumers: [
            {
                "id": "c1",
                "name": "hosein",
                "productCounts": {
                    "a": 26,
                    "b": 30,
                    "c": 41
                },
                "money": 7928
            }
        ],
        resourcePerMonth: 0,
        moneyPerMonth: 0,
        currentMonth: 0,
        months: [getNewMonth()]
    };
    storage.setItemSync(DATA, data);
}

function getNewMonth() {
    return {
        transactions: [],
        productPlans: {},
        overall: {
            a: {
                totalCount: 0,
                totalCost: 0
            },
            b: {
                totalCount: 0,
                totalCost: 0
            },
            c: {
                totalCount: 0,
                totalCost: 0
            }
        }
    };
}

//------------------------------------------------------------- web socket
var sockets = [];

io.on('connection', function (socket) {
    sockets.push(socket);

    socket.emit('data', data);

    socket.on('next-month', function () {
        nextMonth();
        pushData();
    });

    socket.on('plan', function (plan) {
        producerPlan(plan);
        pushData();
    });

    socket.on('transaction', function (t) {
        transaction(t);
        pushData();
    });

});

function pushData() {
    for (var i in sockets) {
        var socket = sockets[i];
        socket.emit('data', data);
    }

}

//------------------------------------------------------------- logic

function nextMonth() {
    console.log('next-month');
    var data = storage.getItem(DATA);
    var i;
    var consumers = data.consumers;
    var producers = data.producers;

    for (i in consumers) {
        var consumer = consumers[i];
        consumer.money += data.moneyPerMonth;
    }

    for (i in producers) {
        var producer = producers[i];
        var producePlan = producer.producePlan;
        var produceCounts = producer.productCounts;
        produceCounts.a += producePlan.a;
        produceCounts.b += producePlan.b;
        produceCounts.c += producePlan.c;

        data.months[data.currentMonth].productPlans[producer.id] = producer.producePlan;
    }

    data.months.push(getNewMonth());
    data.currentMonth++;

    storage.setItemSync(DATA, data);
}

function producerPlan(plan) {
    console.log('plan', plan);

    var data = storage.getItem(DATA);

    var producers = data.producers;
    var producer;
    var id = plan.id;
    var find = false;

    for (var i in producers) {
        producer = producers[i];
        if (producer.id === id) {
            find = true;
            break;
        }
    }

    if (!find) {
        return;
    }

    producer.producePlan.a = parseInt(plan.a);
    producer.producePlan.b = parseInt(plan.b);
    producer.producePlan.c = parseInt(plan.c);

    data.months[data.currentMonth].productPlans[producer.id] = producer.producePlan;

    storage.setItemSync(DATA, data);
}

function transaction(t) {
    console.log('transaction', t);
    var data = storage.getItem(DATA);

    var producers = data.producers;
    var producer;
    var producerId = t.producerId;

    var consumerId = t.consumerId;


    var find = false;
    var i;

    for (i in producers) {
        producer = producers[i];
        if (producer.id === producerId) {
            find = true;
            break;
        }
    }
    if (!find) {
        return;
    }
    var consumers = data.consumers;
    var consumer;
    find = false;

    for (i in consumers) {
        consumer = consumers[i];
        if (consumer.id === consumerId) {
            find = true;
            break;
        }
    }

    if (!find) {
        return;
    }

    t.a_count = parseInt(t.a_count);
    t.b_count = parseInt(t.b_count);
    t.c_count = parseInt(t.c_count);

    t.a_cost = parseInt(t.a_cost);
    t.b_cost = parseInt(t.b_cost);
    t.c_cost = parseInt(t.c_cost);

    if (producer.productCounts.a < t.a_count || producer.productCounts.b < t.b_count || producer.productCounts.c < t.c_count) {
        console.log('insufficient products');
        return;
    }

    if (t.a_cost * t.a_count + t.b_cost * t.b_count + t.c_cost * t.c_count > consumer.money) {
        console.log('insufficient money');
        return;
    }

    data.months[data.currentMonth].transactions.push(t);

    producer.productCounts.a -= parseInt(t.a_count);
    producer.productCounts.b -= parseInt(t.b_count);
    producer.productCounts.c -= parseInt(t.c_count);

    consumer.productCounts.a += parseInt(t.a_count);
    consumer.productCounts.b += parseInt(t.b_count);
    consumer.productCounts.c += parseInt(t.c_count);

    data.months[data.currentMonth].overall.a.totalCount += t.a_count;
    data.months[data.currentMonth].overall.b.totalCount += t.b_count;
    data.months[data.currentMonth].overall.c.totalCount += t.c_count;

    data.months[data.currentMonth].overall.a.totalCost += t.a_count * t.a_cost;
    data.months[data.currentMonth].overall.b.totalCost += t.b_count * t.b_cost;
    data.months[data.currentMonth].overall.c.totalCost += t.c_count * t.c_cost;

    producer.money += t.a_cost * t.a_count + t.b_cost * t.b_count + t.c_cost * t.c_count;
    consumer.money -= t.a_cost * t.a_count + t.b_cost * t.b_count + t.c_cost * t.c_count;

    storage.setItemSync(DATA, data);
}

//-------------------------------------------------------------
app.use('/static', express.static(__dirname + '/static'));

app.get('/', function (req, res) {
    res.sendFile(__dirname + '/template/index.html');
});

app.get('/management', function (req, res) {
    res.sendFile(__dirname + '/template/management.html');
});

app.get('/producers', function (req, res) {
    res.sendFile(__dirname + '/template/producers.html');
});

app.get('/consumers', function (req, res) {
    res.sendFile(__dirname + '/template/consumers.html');
});

app.get('/scoreboard', function (req, res) {
    res.sendFile(__dirname + '/template/scoreboard.html');
});

app.get('/special', function (req, res) {
    res.sendFile(__dirname + '/template/special.html');
});

httpServer.listen(3000, "0.0.0.0", function () {
    console.log('listening on port 3000');
});
